package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectionManager {
	
	private static final String URL = "jdbc:mysql://localhost:3306/";
	private static final String DB = "board?useSSL=false";
	private static final String ID = "root";
	private static final String PW = "1111";
	
	
	public static Connection getConnection() throws SQLException {
		return DriverManager.getConnection(URL + DB	,ID ,PW);
	}

	public static void close(Connection conn) {
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
	}
	public static void close(Statement stmt) {
		if (stmt != null) {
			try {
				stmt	.close();
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
	}
	public static void close(ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
	}
	public static void rollback(Connection conn) {
		if (conn != null) {
			try {
				conn.rollback();
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
	}
}
