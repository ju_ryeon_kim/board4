package article.servlet;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import article.model.Article;
import article.service.ModifyArticleService;
import article.service.ReadArticleService;
import request.RequestValidator;

public class ModifyServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		String article_id = req.getParameter("article_id");
		Article article = null;

		try {
			article = ReadArticleService.getInstance().selectById(article_id);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

		req.setAttribute("article", article);
		RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/view/modifyArticleForm.jsp");
		dispatcher.forward(req, resp);

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if (!RequestValidator.isMultipart(req)) {
			throw new RuntimeException();
		}
		System.out.println("정상");
		String article_id = req.getParameter("article_id");
		String author = req.getParameter("author");
		String title = req.getParameter("title");
		String content = req.getParameter("content");
		String filename = null;
		Part filePart = req.getPart("file1");
		String savaDir = null;

		Part partFile = req.getPart("file1");
		if (RequestValidator.isFilePart(filePart)) {
			savaDir = req.getServletContext().getRealPath("") + File.separator + "uploadFiles";
		}

		Article article;
		try {
			article = ReadArticleService.getInstance().selectById(article_id);
			Article newArticle = new Article(article_id, author, title, content, article.getAddedAt(), filename);
			ModifyArticleService.getInstance().update(newArticle, filePart, savaDir);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}

		resp.sendRedirect(req.getContextPath() + "/article/list");

	}
}
