package article.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import article.model.Article;
import article.service.ReadArticleService;

public class ReadServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String article_id = req.getParameter("article_id");

		Article article = null;
		
		try {
			article = ReadArticleService.getInstance().selectById(article_id);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

		req.setAttribute("article", article);
		RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/view/readArticle.jsp");
		dispatcher.forward(req, resp);
	}
}
