package article.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import article.model.Article;
import article.model.ArticlePage;
import article.service.ListArticleService;
import jdbc.ConnectionManager;

public class ListServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// 현재페이지 넘버를 구한다.

		String curPageVal = req.getParameter("curPage");
		int curPage = 1;
		if (curPageVal != null) {
			curPage = Integer.parseInt(curPageVal);
		}
		try {
			ArticlePage articlePage = ListArticleService.getInstance().getArticlePage(curPage);
			// conn = ConnectionManager.getConnection();
			// pstmt1 = conn.prepareStatement("SELECT COUNT(article_id) FROM
			// articles");
			// rs1 = pstmt1.executeQuery();
			// if (rs1 != null && rs1.next()) {
			// articleCount = rs1.getInt(1);
			// } else {
			// throw new SQLException();
			// }
			//
			// pstmt2 = conn.prepareStatement("SELECT * FROM articles ORDER BY
			// article_id DESC LIMIT ?, ?");
			// pstmt2.setInt(1, (curPage - 1) * articlesPerPage);
			// pstmt2.setInt(2, articlesPerPage);
			// rs2 = pstmt2.executeQuery();
			// if (rs2 == null) {
			// throw new SQLException();
			// }
			// while (rs2.next()) {
			// Article article = new Article(rs2.getString("article_id"),
			// rs2.getString("author"), rs2.getString("title"),
			// rs2.getString("content"),
			// rs2.getTimestamp("added_at").toLocalDateTime(),
			// rs2.getString("filename"));
			// articles.add(article);
			// }
			// ArticlePage articlePage = new ArticlePage(curPage, articleCount,
			// pagesPerBlock, articlesPerPage, articles);
			req.setAttribute("articlePage", articlePage);

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/view/listArticleForm.jsp");
		dispatcher.forward(req, resp);

	}
}
