package article.servlet;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import article.model.Article;
import article.service.FileService;
import article.service.WriteArticleService;
import request.RequestValidator;

public class WriteServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/view/writeArticleForm.jsp");
		dispatcher.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// 1. 멀티파트 체크
		if (!RequestValidator.isMultipart(req)) {
			throw new RuntimeException();
		}

		String author = req.getParameter("author");
		String title = req.getParameter("title");
		String content = req.getParameter("content");
		LocalDateTime dateTime = LocalDateTime.now();
		Part filePart = req.getPart("file1");
		String savaDir = null;

		if (RequestValidator.isFilePart(filePart)) {
			savaDir = req.getServletContext().getRealPath("") + File.separator + "uploadFiles";
		}
		Article article = new Article(null, author, title, content, dateTime, null);

		try {
			int article_id = WriteArticleService.getInstance().insert(article, filePart, savaDir);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}

		resp.sendRedirect(req.getContextPath() + "/article/list");

	}
}
