package article.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import article.service.FileService;
import jdbc.ConnectionManager;

public class DownloadServlet extends HttpServlet {
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String article_id = req.getParameter("article_id");
		
		try {
			FileService.getInstance().downlaod(article_id, req, resp);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

//		Connection conn = null;
//		PreparedStatement pstmt = null;
//		ResultSet rs = null;
//		String filename = null;
//		FileInputStream fis = null;
//		OutputStream out = null;
//		try {
//			conn = ConnectionManager.getConnection();
//			pstmt = conn.prepareStatement("SELECT * FROM articles WHERE article_id = ?");
//			pstmt.setString(1, article_id);
//			rs = pstmt.executeQuery();
//			if (rs.next()) {
//				filename = rs.getString("filename");
//			}
//
//			String dir = req.getServletContext().getRealPath("/") + File.separator + "uploadFiles";
//			String filePath = dir + File.separator + filename;
//
//			// 마임타입 확인
//			String mimetype = req.getServletContext().getMimeType(filePath);
//			if (mimetype == null) {
//				mimetype = "application/octec-stream";
//			}
//
//			// 파일읽기준비'
//			File downloadFile = new File(filePath);
//			fis = new FileInputStream(downloadFile);
//
//			// response 변경
//			resp.setContentType(mimetype);
//			resp.setContentLength((int) downloadFile.length());
//
//			// 다운로드 강제
//			resp.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"",
//					URLEncoder.encode(filename.substring(filename.indexOf("_") + 1), "utf-8")));
//
//			// 파일쓰기준비
//			out = resp.getOutputStream();
//
//			byte[] buffer = new byte[fis.available()];
//			int bytesRead = -1;
//			while ((bytesRead = (fis.read(buffer))) != -1) {
//				out.write(buffer, 0, bytesRead);
//			}
//
//		} catch (SQLException e) {
//			e.printStackTrace();
//			throw new RuntimeException(e);
//		} finally {
//			if (fis != null) {
//				fis.close();
//			}
//			if (out != null) {
//				out.close();
//			}
//			ConnectionManager.close(rs);
//			ConnectionManager.close(pstmt);
//			ConnectionManager.close(conn);
//		}

	}
}
