package article.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import article.service.DeleteService;

public class DeleteServlet extends HttpServlet {
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String article_id = req.getParameter("article_id");
		String savaDir = req.getServletContext().getRealPath("/") + "uploadFiles";
		try {
			DeleteService.getInstance().delete(article_id, savaDir);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		resp.sendRedirect(req.getContextPath()+"/article/list");
	}
}
