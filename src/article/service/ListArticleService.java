package article.service;

import java.sql.SQLException;
import java.util.List;

import article.dao.ArticleDao;
import article.model.Article;
import article.model.ArticlePage;

public class ListArticleService {
	private static ListArticleService instance = new ListArticleService();

	private ListArticleService() {
	}

	public static ListArticleService getInstance() {
		return instance;
	}
	private int pagesPerBlock = 5;
	private int articlesPerPage = 10;

	public ArticlePage getArticlePage(int curPage) throws SQLException {
		ArticlePage articlePage = null;
		
		int articleCount = ArticleDao.getInstance().count();
		List<Article> articles;
		try {
			articles = ArticleDao.getInstance().select((curPage - 1)* articlesPerPage, articlesPerPage);
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		}
		articlePage = new ArticlePage(curPage, articleCount, pagesPerBlock, articlesPerPage, articles);
		return articlePage;
	}


}
