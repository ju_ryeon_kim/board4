package article.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import article.dao.ArticleDao;
import article.model.Article;

public class FileService {
	private static FileService instance = new FileService();

	private FileService() {
	}

	public static FileService getInstance() {
		return instance;
	}

	public int save(Article article, Part filePart, String saveDir) throws IOException, SQLException {
		String filename = UUID.randomUUID() + "_" + URLDecoder.decode(filePart.getSubmittedFileName(), "utf-8");
		File saveDirF = new File(saveDir);
		int result = -1;
		if (!saveDirF.exists()) {
			saveDirF.mkdir();
		}
		try {
			filePart.write(saveDir + File.separator + filename);
			filePart.delete();

			result = ArticleDao.getInstance().update(new Article(article.getId(), article.getAuthor(), article.getTitle(),
					article.getContent(), article.getAddedAt(), filename));
		} catch (IOException | SQLException e) {
			e.printStackTrace();
			throw e;
		} 
		return result;
	}

	public void downlaod(String article_id, HttpServletRequest req, HttpServletResponse resp) throws SQLException, IOException {
		Article article = null;
		try {
			article = ArticleDao.getInstance().selectById(article_id);
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		}
		String filename = article.getFilename();
		String dir = req.getServletContext().getRealPath("/") + File.separator + "uploadFiles";
		String filePath = dir + File.separator + filename;
		FileInputStream fis = null;
		OutputStream out = null;
		try {

			// 마임타입 확인
			String mimetype = req.getServletContext().getMimeType(filePath);
			if (mimetype == null) {
				mimetype = "application/octec-stream";
			}

			// 파일읽기준비'
			File downloadFile = new File(filePath);
			fis = new FileInputStream(downloadFile);

			// response 변경
			resp.setContentType(mimetype);
			resp.setContentLength((int) downloadFile.length());

			// 다운로드 강제
			resp.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"",
					URLEncoder.encode(filename.substring(filename.indexOf("_") + 1), "utf-8")));

			// 파일쓰기준비
			out = resp.getOutputStream();

			byte[] buffer = new byte[fis.available()];
			int bytesRead = -1;
			while ((bytesRead = (fis.read(buffer))) != -1) {
				out.write(buffer, 0, bytesRead);
			}
		} catch (IOException e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (fis != null) {
				fis.close();
			}
			if (out != null) {
				out.close();
			}
		}
	}

	public void delete(String filePath) {
			File file = new File(filePath);
			System.out.println(filePath);
			if (file.exists()) {
				System.out.println("파일존재");
				file.delete();
			}
	}
}
