package article.service;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.http.Part;

import article.dao.ArticleDao;
import article.model.Article;
import request.RequestValidator;

public class ModifyArticleService {
	private static ModifyArticleService instance = new ModifyArticleService();

	private ModifyArticleService() {
	}

	public static ModifyArticleService getInstance() {
		return instance;
	}

	public int update(Article article, Part filePart, String dir) throws SQLException, IOException {
		int result = -1;
		try {
			result = ArticleDao.getInstance().update(article);
			String filename = null;
			//업로드 파일이 있을경우 파일을 저장하고 업데이트
			if (RequestValidator.isFilePart(filePart)) {
				Article newArticle = new Article(article.getId(), article.getAuthor(), article.getTitle(),
						article.getContent(), article.getAddedAt(), null);
				FileService.getInstance().save(newArticle, filePart, dir);
			}
		} catch (SQLException | IOException e) {
			e.printStackTrace();
			throw e;
		}
		return result;
	}
}
