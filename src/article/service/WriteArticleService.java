package article.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;

import javax.servlet.http.Part;

import article.dao.ArticleDao;
import article.model.Article;
import request.RequestValidator;

public class WriteArticleService {
	private static WriteArticleService instance = new WriteArticleService();

	private WriteArticleService() {
	}

	public static WriteArticleService getInstance() {
		return instance;
	}
	

	public int insert(Article article, Part filePart, String dir) throws UnsupportedEncodingException, SQLException, IOException{
		int article_id = -1;
		try {
			//파일을 제외한 정보 DB저장
			article_id = ArticleDao.getInstance().insert(article);

			String filename = null;
			//업로드 파일이 있을경우 파일을 저장하고 업데이트
			if (RequestValidator.isFilePart(filePart)) {
				Article newArticle = new Article(String.valueOf(article_id), article.getAuthor(), article.getTitle(),
						article.getContent(), article.getAddedAt(), null);
				FileService.getInstance().save(newArticle, filePart, dir);
			}
		} catch (SQLException | IOException e) {
			e.printStackTrace();
			throw e;
		}
		return article_id;
	}

}
