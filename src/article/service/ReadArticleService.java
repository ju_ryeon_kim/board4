package article.service;

import java.sql.SQLException;

import article.dao.ArticleDao;
import article.model.Article;

public class ReadArticleService {
	private static ReadArticleService instance = new ReadArticleService();

	private ReadArticleService() {
	}

	public static ReadArticleService getInstance() {
		return instance;
	}
	

	public Article selectById(String article_id) throws SQLException {
		Article article = null;
		try {
			article = ArticleDao.getInstance().selectById(article_id);
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		}
		return article;
	}

}
