package article.service;

import java.io.File;
import java.sql.SQLException;

import article.dao.ArticleDao;
import article.model.Article;

public class DeleteService {
	private static DeleteService instance = new DeleteService();

	private DeleteService() {
	}

	public static DeleteService getInstance() {
		return instance;
	}

	public void delete(String article_id, String savaDir) throws SQLException {
		try {
			Article article = ArticleDao.getInstance().selectById(article_id);
			if (article.getFilename() != null) {
				FileService.getInstance().delete(savaDir + File.separator + article.getFilename());
			}
			ArticleDao.getInstance().delete(article_id);

		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		}
	}
}
