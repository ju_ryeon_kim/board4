package article.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import article.model.Article;
import jdbc.ConnectionManager;

public class ArticleDao {

	private static ArticleDao instance = new ArticleDao();

	private ArticleDao() {
	}

	public static ArticleDao getInstance() {
		return instance;
	}

	public int insert(Article article) throws SQLException {
		Connection conn = null;
		PreparedStatement pstmt = null;
		Statement stmt = null;
		ResultSet rs = null;
		int result = -1;
		try {
			conn = ConnectionManager.getConnection();
			pstmt = conn.prepareStatement("INSERT INTO articles (author, title, content, added_at) VALUES(?, ?, ?, ?)");
			conn.setAutoCommit(false);
			pstmt.setString(1, article.getAuthor());
			pstmt.setString(2, article.getTitle());
			pstmt.setString(3, article.getContent());
			pstmt.setTimestamp(4, Timestamp.valueOf(article.getAddedAt()));
			pstmt.executeUpdate();

			stmt = conn.createStatement();
			rs = stmt.executeQuery("SELECT LAST_INSERT_ID() FROM articles");
			if (rs.next()) {
				result = rs.getInt(1);
			}
			conn.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		} finally {
			ConnectionManager.close(rs);
			ConnectionManager.close(stmt);
			ConnectionManager.close(pstmt);
			ConnectionManager.close(conn);
		}
		return result;
	}

	public int update(Article article) throws SQLException {
		Connection conn = null;
		PreparedStatement pstmt = null;
		int result = -1;
		try {
			conn = ConnectionManager.getConnection();
			pstmt = conn.prepareStatement("UPDATE articles SET author=?, title=?, content=?, filename=? WHERE article_id=?");
			pstmt.setString(1, article.getAuthor());
			pstmt.setString(2, article.getTitle());
			pstmt.setString(3, article.getContent());
			pstmt.setString(4, article.getFilename());
			pstmt.setString(5, article.getId());
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		} finally {
			ConnectionManager.close(pstmt);
			ConnectionManager.close(conn);
		}
		return result;
	}

	public Article selectById(String article_id) throws SQLException {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Article article = null;
		try {
			conn = ConnectionManager.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM articles WHERE article_id = ?");
			pstmt.setString(1, article_id);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				article = new Article(rs.getString("article_id"), rs.getString("author"), rs.getString("title"),
						rs.getString("content"), rs.getTimestamp("added_at").toLocalDateTime(), rs.getString("filename"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		} finally {
			ConnectionManager.close(rs);
			ConnectionManager.close(pstmt);
			ConnectionManager.close(conn);
		}
		return article;
	}

	public int count() throws SQLException {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int articleCount = -1;
		try {
			conn = ConnectionManager.getConnection();
			pstmt = conn.prepareStatement("SELECT COUNT(article_id) FROM articles");
			rs = pstmt.executeQuery();
			if (rs != null && rs.next()) {
				articleCount = rs.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		} finally {
			ConnectionManager.close(rs);
			ConnectionManager.close(pstmt);
			ConnectionManager.close(conn);
		}
		return articleCount;
	}

	public List<Article> select(int offset, int limit) throws SQLException {
		List<Article> articles = new ArrayList<>();
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		try {
			conn = ConnectionManager.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM articles ORDER BY article_id DESC LIMIT ?, ?");
			pstmt.setInt(1, offset);
			pstmt.setInt(2, limit);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Article article = new Article(rs.getString("article_id"), rs.getString("author"), rs.getString("title"),
						rs.getString("content"), rs.getTimestamp("added_at").toLocalDateTime(), rs.getString("filename"));
				articles.add(article);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		} finally {
			ConnectionManager.close(pstmt);
			ConnectionManager.close(conn);
		}

		return articles;
	}

	public void delete(String article_id) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = ConnectionManager.getConnection();
			pstmt = conn.prepareStatement("DELETE FROM articles WHERE article_id=?");
			pstmt.setString(1, article_id);
			int affectedRow = pstmt.executeUpdate();
			if (affectedRow < 0) {
				throw new SQLException();
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			ConnectionManager.close(rs);
			ConnectionManager.close(pstmt);
			ConnectionManager.close(conn);
		}

	}

}
