package article.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Article {
	private String id;
	private String author;
	private String title;
	private String content;
	private LocalDateTime addedAt;
	private String filename;


	public Article(String id, String author, String title, String content, LocalDateTime addedAt, String filename) {
		this.id = id;
		this.author = author;
		this.title = title;
		this.content = content;
		this.addedAt = addedAt;
		this.filename = filename;
	}

	public LocalDateTime getAddedAt() {
		return addedAt;
	}
	
	public String getAddedAtFormatted() {
		return addedAt.format(DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss"));
	}

	public String getId() {
		return id;
	}

	public String getAuthor() {
		return author;
	}

	public String getTitle() {
		return title;
	}

	public String getContent() {
		return content;
	}

	public String getFilename() {
		return filename;
	}
	
	public String getFilenameFormatted() {
		return filename.substring(filename.indexOf("_") + 1);
	}
}
