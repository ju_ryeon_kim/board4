package article.model;

import java.util.List;

public class ArticlePage {
	int curPage = 1;
	int articleCount = -1;
	int pagesPerBlock = 5;
	int articlesPerPage = 10;
	int pageCount;
	int startPage;
	int endPage;
	List<Article> articles;

	public ArticlePage(int curPage, int articleCount, int pagesPerBlock, int articlesPerPage, List<Article> articles) {
		this.curPage = curPage;
		this.articleCount = articleCount;
		this.pagesPerBlock = pagesPerBlock;
		this.articlesPerPage = articlesPerPage;
		this.articles = articles;
		
		pageCount = (int) Math.ceil((double)articleCount / (double)articlesPerPage);
		startPage = curPage/pagesPerBlock * pagesPerBlock + 1;
		if ((curPage % pagesPerBlock) == 0) startPage =  startPage - pagesPerBlock;
		endPage = startPage + (pagesPerBlock - 1);
		if (endPage > pageCount) {
			endPage = pageCount;
		}
	}

	public int getCurPage() {
		return curPage;
	}
	public int getArticleCount() {
		return articleCount;
	}
	public int getPagesPerBlock() {
		return pagesPerBlock;
	}
	public int getArticlesPerPage() {
		return articlesPerPage;
	}
	public int getPageCount() {
		return pageCount;
	}
	public int getStartPage() {
		return startPage;
	}
	public int getEndPage() {
		return endPage;
	}
	public List<Article> getArticles() {
		return articles;
	}
}
