package context;

public class JdbcNotRoadedException extends RuntimeException {

	public JdbcNotRoadedException(Throwable e) {
		super(e);
	}

}
