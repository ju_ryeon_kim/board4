package context;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class ContextLoaderListener implements ServletContextListener {

	private String driver = "com.mysql.jdbc.Driver";
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void contextInitialized(ServletContextEvent arg0) {
		try {
			System.out.println(getClass().getName() + ": " +driver);
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
//			드라이버가 로드 되지 못했다면 어떻게 처리하는게 좋을까?
		}
	}
}
