package request;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

public class RequestValidator {
	public static boolean isMultipart(HttpServletRequest req) {
		return req.getContentType().startsWith("multipart/");
	}

	public static boolean isFilePart(Part filePart) {
		return filePart != null && filePart.getHeader("Content-Disposition").contains("filename=") && filePart.getSize() > 0;
	}
}
