<%@page import="article.model.Article"%>
<%@page import="article.model.ArticlePage"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<table>
		<tr>
			<td colspan="4"><a href="/Board4/article/write">[글쓰기]</a></td>
		</tr>
		<tr>
			<td>번호</td>
			<td>제목</td>
			<td>작성자</td>
			<td>작성일자</td>
		</tr>
		<c:forEach var="article" items="${articlePage.articles }">
			<tr>
				<td>${article.id }</td>
				<td><a href="/Board4/article/read?article_id=${article.id }">${article.title }</a></td>
				<td>${article.author }</td>
				<td>${article.addedAtFormatted }</td>
			</tr>
		</c:forEach>
		<tr>
			<td colspan="4"><c:if
					test="${articlePage.startPage > articlePage.pagesPerBlock }">
					<a
						href="/Board4/article/list?curPage=${articlePage.startPage - 1 }">[이전]</a>
				</c:if> <c:forEach var="pageNo" begin="${articlePage.startPage}"
					end="${articlePage.endPage }">
					<a href="/Board4/article/list?curPage=${pageNo }">${pageNo }</a>
				</c:forEach> <c:if test="${articlePage.endPage < articlePage.pageCount }">
					<a href="/Board4/article/list?curPage=${articlePage.endPage + 1 }">[다음]</a>
				</c:if></td>
		</tr>
	</table>
</body>
</html>