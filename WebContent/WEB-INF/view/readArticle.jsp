<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<p>제목:${article.title }</p>
	<p>작성자:${article.author }</p>
	<p>
		내용:<span>${article.content }</span>
	</p>
	<p>
		파일:
		<c:if test="${ ! empty article.filename}">
			<a href="/Board4/article/file?article_id=${article.id }">${article.filenameFormatted}</a>
		</c:if>
	</p>
	<a href="/Board4/article/modify?article_id=${article.id }">[수정]</a>
	<a href="/Board4/article/delete?article_id=${article.id }">[삭제]</a>
</body>
</html>